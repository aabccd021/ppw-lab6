from django import forms


class Status_Form(forms.Form):
    title = forms.CharField(
        label='isi',
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control',
                   'name': 'the-form'}
        )
    )
