from django.test import TestCase, Client
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
# Create your tests here.
from selenium.webdriver.common.keys import Keys

from lab6.forms import Status_Form
from lab6.models import Status
from lab6.views import index, profile


class Lab6UnitTest(TestCase):

    def test_lab_6_url_is_exist(self):
        response = Client().get('/lab-6/')
        self.assertEqual(response.status_code, 200)

    def test_lab_6_using_index_func(self):
        found = resolve('/lab-6/')
        self.assertEqual(found.func, index)

    def test_lab_6_using_main_page_template(self):
        response = Client().get('/lab-6/')
        self.assertTemplateUsed(response, 'main.html')

    def test_lab_6_render_the_result(self):
        test = 'Hello apa kabar?'
        response = Client().get('/lab-6/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_form_status_has_placeholder(self):
        form = Status_Form()
        self.assertIn('id="id_title"', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'title': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['title'],
            ["This field is required."]
        )

    def test_model_can_create_new_status(self):
        new_status = Status.objects.create(title='mengerjakan lab ppw')
        # Retrieving all available status
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_profile_using_profile_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)

    def test_profile_using_profile_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_profile_render_the_result(self):
        test = 'Halo, Saya Aab'
        response = Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_lab_6_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/lab-6/', {'title': test})
        self.assertEqual(response_post.status_code, 200)

        response = Client().get('/lab-6/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_lab_6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/lab-6/', {'title': ''})
        self.assertEqual(response_post.status_code, 200)

        response = Client().get('/lab-6/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)


class Lab6FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(Lab6FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        selenium.get('https://ppw-lab6.herokuapp.com/lab-6/')

        title = selenium.find_element_by_id('id_title')
        submit = selenium.find_element_by_id('submit')

        title.send_keys('Berhasil menambahkan Todo')
        submit.send_keys(Keys.RETURN)
        assert 'Berhasil menambahkan Todo' in selenium.page_source

    def test_page_title(self):
        selenium = self.selenium
        selenium.get('https://ppw-lab6.herokuapp.com/lab-6/')
        self.assertIn('Story 6', selenium.title)

    def test_heading(self):
        selenium = self.selenium
        selenium.get('https://ppw-lab6.herokuapp.com/lab-6/')
        heading1_text = selenium.find_element_by_tag_name('h1').text
        self.assertEqual('Hello apa kabar?', heading1_text)

    def test_status_color(self):
        selenium = self.selenium
        selenium.get('https://ppw-lab6.herokuapp.com/lab-6/')
        element_color = "rgba(0, 128, 0, 1)"
        a = selenium.find_element_by_tag_name("p")
        color = a.value_of_css_property('color')
        self.assertEqual(color, element_color)

    def test_heading1_color(self):
        selenium = self.selenium
        selenium.get('https://ppw-lab6.herokuapp.com/lab-6/')
        element_color = "rgba(0, 0, 255, 1)"
        a = selenium.find_element_by_tag_name("h1")
        color = a.value_of_css_property('color')
        self.assertEqual(color, element_color)
