from django.urls import path

from lab6 import views

urlpatterns = [
    path('lab-6/',views.index, name='index'),
    path('',views.index, name='index'),
    path('profile/',views.profile, name='profile')
]
