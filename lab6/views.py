from django.shortcuts import render

from lab6.forms import Status_Form
from lab6.models import Status


def index(request):
    form = Status_Form()
    statues = Status.objects.all().values()
    if request.method == 'POST':
        new_status = request.POST['title']
        Status(title=new_status).save()
    return render(request, 'main.html', {'form': form, 'statues': statues})

def profile(request):
    return render(request, 'profile.html')

# Create your views here.
